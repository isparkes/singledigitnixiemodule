#include <Wire.h>

// ===========================================================================
// I2C Interface definition
#define I2C_SLAVE_ADDR                0x30

#define I2C_VALUE                     0x00
#define I2C_DIM                       0x01
#define I2C_FADE                      0x02
#define I2C_SCROLL                    0x03
#define I2C_MODE                      0x04
#define I2C_RGB_BL                    0x05

// ===========================================================================
// Display handling
#define DIGIT_DISPLAY_COUNT   256                     // Switch off the digit at the end by default
#define DIGIT_DISPLAY_ON      0                       // Switch on the digit at the beginning by default
#define DIGIT_DISPLAY_OFF     255                     // Switch off the digit at the end by default
#define DIGIT_DISPLAY_NEVER   -1                      // this count will never happen

// Dimming value
const byte DIM_VALUE = DIGIT_DISPLAY_COUNT / 8;

// Display mode, set per digit
#define BLANKED  0
#define DIMMED   1
#define FADE     2
#define NORMAL   3
#define BLINK    4
#define SCROLL   5
#define BRIGHT   6

// ===========================================================================
// dsiplay variables
byte loopCount = 0;
byte previousValue = 0;
byte currentValue = 0;
boolean dpSet = false;
byte dispType = FADE;
byte fadeSteps = 255;
float fadeStep = ((float) DIGIT_DISPLAY_COUNT + 1) / ((float) fadeSteps);
byte scrollSteps = 150;
byte fadeState = 0;
byte RGBCounter = 0;
byte RVal = 0;
byte GVal = 0;
byte BVal = 0;
byte digitOffCount = DIGIT_DISPLAY_OFF;
boolean blinkState = true;

int digitOnTime;
int digitOffTime;
int digitSwitchTime;

// Output pin mappings
// PD0 (pin30) digit 0
// PD1 (pin31) digit 1
// PD2 (pin32) digit 2
// PD3 (pin1)  digit 3
// PD4 (pin2)  digit 4
// PD5 (pin9)  digit 5
// PD6 (pin10) digit 6
// PD7 (pin11) digit 7
// PB0 (pin12) digit 8
// PB1 (pin13) digit 9
// PB2 (pin14) decimal point

// RGB
// PB3 (pin15) R / MOSI
// PB4 (pin16) G / MISO
// PB5 (pin17) B / SCK

// Input
// PC0 (pin23) Address 0
// PC1 (pin24) Address 1
// PC2 (pin25) Address 2
// PC3 (pin26) Address 3
// PC4 (pin27) SDA
// PC5 (pin28) SCL
// PC6 (pin29) RESET

// the setup routine runs once when you press reset:
void setup() {
  // initialize PORT D and B as output.
  DDRD = 0xff;
  DDRB = 0xff;

  // Initialise port C as input, with pullups
  DDRC = 0x0;
  PORTC = 0xff;

  // Speed up internal clock
  //OSCCAL = 255;

  // read the address pins
  byte address = PORTC & 0xf;

  // Start the RTC communication
  Wire.begin(I2C_SLAVE_ADDR + address);
  Wire.onReceive(receiveEvent);

  // Enable broadcasts
  TWAR = TWAR | 1;  // enable broadcasts to be received

  setOnOffTimes();
}

// the loop routine runs over and over again forever
void loop() {
  loopCount++;
  if (loopCount == 100) {
    blinkState = !blinkState;
    loopCount = 0;
    setOnOffTimes();
  }

  outputDisplay();
}

/**
   Perform a single complete display impression
*/
void outputDisplay() {

  byte tmpDispType = dispType;

  if (dispType = SCROLL)
  
  // manage fading/scrolling
  // Scrolling: we show each digit scrollSteps number of times and then
  // count down by 1
  switch (tmpDispType) {
    case SCROLL:
      {
        if ((previousValue != currentValue) && (currentValue == 0)) {
          digitSwitchTime = digitOnTime;
          if (fadeState == 0) {
            fadeState = scrollSteps;
          } else if (fadeState == 1) {
            fadeState = 0;
            previousValue = previousValue - 1;
          } else {
            fadeState = fadeState - 1;
          }
        }
        break;
      }

    // Fading: for each impression we show 1 fade step less of the old
    // digit and 1 fade step more of the new
    case FADE:
      {
        if (currentValue != previousValue) {
          if (fadeState == 0) {
            // Start the fade
            fadeState = fadeSteps;
            digitSwitchTime = digitOnTime + 1;
          } else if (fadeState == 1) {
            // finish the fade
            fadeState = 0;
            previousValue = currentValue;
          } else {
            // Continue the fade
            fadeState--;
            digitSwitchTime = (byte) fadeState * fadeStep;
          }
        } else {
          digitSwitchTime = DIGIT_DISPLAY_NEVER;
        }
      }
    default:
      {
      }
  }

  // do a complete impression
  // On Switch and off should not ever have the same value
  for (int timer = 0 ; timer < 256 ; timer++) {
    if (timer == digitOnTime) {
      setDigitOut(previousValue);
    }

    if (timer == digitSwitchTime) {
      setDigitOut(currentValue);
    }

    if (timer == digitOffTime) {
      setDigitOut(0xff);
    }

    // bit banged RGB PWM
    outputRGB(timer);
  }
}

void setOnOffTimes() {
  // Select the brightness of the display
  switch (dispType) {
    case BLANKED:
      {
        digitOnTime = DIGIT_DISPLAY_NEVER;
        digitOffTime = DIGIT_DISPLAY_OFF;
        break;
      }
    case DIMMED:
      {
        digitOnTime = DIGIT_DISPLAY_ON;
        digitOffTime = DIM_VALUE;
        break;
      }
    case BRIGHT:
      {
        digitOnTime = DIGIT_DISPLAY_ON;
        digitOffTime = DIGIT_DISPLAY_NEVER;
        break;
      }
    case FADE:
    case NORMAL:
    case SCROLL:
      {
        digitOnTime = DIGIT_DISPLAY_ON;
        if (digitOffCount == 255) {
          digitOffTime = DIGIT_DISPLAY_NEVER;
        } else {
          digitOffTime = digitOffCount;
        }
        break;
      }
    case BLINK:
      {
        if (blinkState) {
          digitOnTime = DIGIT_DISPLAY_ON;
          digitOffTime = digitOffCount;
        } else {
          digitOnTime = DIGIT_DISPLAY_NEVER;
          digitOffTime = DIGIT_DISPLAY_ON;
        }
        break;
      }
  }
}

/*
   set the port outputs according to the digit we are showing.
   If we get 0xFF we turn all digit outputs off (but not the DP).
*/
void setDigitOut(byte digit) {
  byte portb = PORTB;
  portb & 0xFC;

  if (dpSet) {
    portb = portb | 0x04;
  }

  if (digit == 0xff) {
    PORTB = portb;
    PORTD = 0;
  } else {
    int value = 1 << digit;
    byte portd = value & 0xff;
    PORTD = portd;
    byte hibyte = (value >> 8) & 0x3;
    portb = portb | hibyte;
    PORTB = portb;
  }
}

/**
   bit bang RGB outputs - we don't have enough PWM counters to do this using the timers
*/
void outputRGB(byte RGBCounter) {
  byte portb = PORTB;
  if (RGBCounter == 0) {
    portb = portb | 0x38;       // PB3, PB4, PB5 all on  --> MSB 0011 1000 LSB
  }

  if (RGBCounter == RVal) {
    portb = portb & 0xF7;       // PB3 off               --> MSB 1111 0111 LSB
  }

  if (RGBCounter == GVal) {
    portb = portb & 0xEF;       // PB4 off               --> MSB 1110 1111 LSB
  }

  if (RGBCounter == BVal) {
    portb = portb & 0xDF;       // PB5 off               --> MSB 1101 1111 LSB
  }

  PORTB = portb;
}

//**********************************************************************************
//**********************************************************************************
//*                                 I2C interface                                  *
//**********************************************************************************
//**********************************************************************************

//  operation       params        description
//  0x00            1             Set value: display this value
//  0x01            1             Set dimming: set dimming to given value. 0 = off, 255 = full brightness
//  0x02            1             Set fade steps
//  0x03            1             Set scroll steps
//  0x04            1             Set display mode, see mode definition above
//  0x05            3             Set RGB backlights

/**
   receive information from the master
*/
void receiveEvent(int bytes) {
  // the operation tells us what we are getting
  int operation = Wire.read();

  // only accept the value if it is between 0 and 9, after stripping the MSB (which is the DP value)
  if (operation == I2C_VALUE) {
    byte newValue = Wire.read();
    boolean newSetDP = ((newValue && 0x80) > 0);
    newValue = newValue & 0x7f;

    if ((newValue >= 0) && (newValue <= 9)) {
      previousValue = currentValue;
      currentValue = newValue;
      dpSet = newSetDP;
    }
  }

  if (operation == I2C_DIM) {
    digitOffCount = Wire.read();
    fadeStep = ((float) digitOffCount) / ((float) fadeSteps);
    setOnOffTimes();
  }

  // We can't accept a 0 value for the new fadesteps value
  if (operation == I2C_FADE) {
    byte newFadeSteps = Wire.read();
    fadeSteps = newFadeSteps;
    fadeStep = ((float) digitOffCount) / ((float) fadeSteps);
    setOnOffTimes();
  }

  // We can't accept a 0 value for the new scrollteps value
  if (operation == I2C_SCROLL) {
    byte newScrollSteps = Wire.read();

    if (newScrollSteps > 0) {
      scrollSteps = newScrollSteps;
      setOnOffTimes();
    }
  }

  // if we get an invalid mode, ignore it
  if (operation == I2C_MODE) {
    byte newMode = Wire.read();
    if ((newMode >= BLANKED) && (newMode <= BRIGHT)) {
      dispType = newMode;
      setOnOffTimes();
    }
  }

  if (operation == I2C_RGB_BL) {
    RVal = Wire.read();
    GVal = Wire.read();
    BVal = Wire.read();
  }
}
