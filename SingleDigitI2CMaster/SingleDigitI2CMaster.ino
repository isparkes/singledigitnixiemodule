#include <Wire.h>

void setup() {
  Serial.begin(115200);
  Wire.begin(); // join i2c bus (address optional for master)

  Wire.beginTransmission(0x3f);
  Wire.write(4);      // set mode
  //Wire.write(1);      // dimmed - OK
  Wire.write(2);      // fade - OK
  //Wire.write(3);      // normal - OK
  //Wire.write(4);      // blink - OK
  //Wire.write(5);      // scroll - NOK
  int result = Wire.endTransmission();

  Wire.beginTransmission(0x3f);
  Wire.write(1);      // Set dim
  Wire.write(255);    // full brightness
  result = Wire.endTransmission();
}

byte x = 0;
byte y = 0;
byte dim = 255;

void loop() {

  Wire.beginTransmission(0x3f);
  Wire.write(0);
  Wire.write(x);
  int result = Wire.endTransmission();

  x++;
  if (x > 9) {
    x = 0;
        Wire.beginTransmission(0x3f);
        Wire.write(1);
        Wire.write(dim);
        result = Wire.endTransmission();
        dim = dim - 10;
  }

  y += 4;

  Wire.beginTransmission(0x3f); // transmit to device #8
  Wire.write(5);              // sends one byte
  Wire.write(y);              // sends one byte
  Wire.write(256-y);              // sends one byte
  Wire.write(y);              // sends one byte
  result = Wire.endTransmission();    // stop transmitting

  delay(1000);
}
